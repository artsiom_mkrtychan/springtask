package com.epam.constants;


public interface Constants {
    String ERROR = "error";
    String SINGED = "singed";
    String INDEX_VIEW = "index";
    String LOGIN = "login";
    String GREETING_VIEW = "greeting";
    String USER_NOT_FOUND_MSG = "User not found";
    String DELIMITER = ",";
    String INDEX_PAGE = "/";
    int COOKIE_LIFE_TIMEOUT = 3600;
}
