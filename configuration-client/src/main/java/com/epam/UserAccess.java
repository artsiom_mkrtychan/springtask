package com.epam;

import com.epam.constants.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RefreshScope
@Component
@RestController
public class UserAccess implements Constants {

    @Value("${users.logins}")
    String logins;

    @Value("${users.passwords}")
    String passwords;

    @RequestMapping(value = "/singout")
    public void singOut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cookie singedCookie = new Cookie(SINGED, null);
        Cookie loginCookie = new Cookie(LOGIN, null);

        singedCookie.setHttpOnly(true);
        loginCookie.setHttpOnly(true);
        singedCookie.setMaxAge(0);
        loginCookie.setMaxAge(0);
        response.addCookie(singedCookie);
        response.addCookie(loginCookie);

        response.sendRedirect(INDEX_PAGE);
    }


    @RequestMapping(value = "/")
    public ModelAndView index(
            @RequestParam(name = "login", required = false) String login,
            @RequestParam(name = "password", required = false) String password,
            @RequestParam(name = "rememberme", required = false) boolean rememberme,
            HttpServletRequest request,
            HttpServletResponse response) {

        ModelAndView model;
        Cookie singedCookie = WebUtils.getCookie(request, SINGED);
        Cookie loginCookie = WebUtils.getCookie(request, LOGIN);

        if (login != null && password != null) {
            if (userExist(login, password)) {
                model = new ModelAndView(GREETING_VIEW);
                model.addObject(LOGIN, login);

                if (rememberme) {
                    singedCookie = new Cookie(SINGED, SINGED);
                    loginCookie = new Cookie(LOGIN, login);

                    singedCookie.setHttpOnly(true);
                    loginCookie.setHttpOnly(true);

                    singedCookie.setMaxAge(COOKIE_LIFE_TIMEOUT);
                    loginCookie.setMaxAge(COOKIE_LIFE_TIMEOUT);

                    response.addCookie(singedCookie);
                    response.addCookie(loginCookie);
                }

            } else {
                model = new ModelAndView(INDEX_VIEW);
                model.addObject(ERROR, USER_NOT_FOUND_MSG);
            }
        } else if (singedCookie == null) {
            model = new ModelAndView(INDEX_VIEW);
        } else {
            model = new ModelAndView(GREETING_VIEW);
            model.addObject(LOGIN, loginCookie.getValue());
        }

        return model;
    }


    private boolean userExist(String login, String password) {
        String[] loginsArray = logins.split(DELIMITER);
        String[] passwordsArray = passwords.split(DELIMITER);

        for (int i = 0; i < loginsArray.length; i++) {
            if (loginsArray[i].equals(login)) {
                return passwordsArray[i].equals(password);
            }
        }
        return false;
    }
}
